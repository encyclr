from django.conf.urls.defaults import *

urlpatterns = patterns('encyclr.dur.views',
    (r'^$', 'index'),
    (r'^i$', 'login'),
    (r'^o$', 'logout'),
    (r'^s$', 'newuser'),
    (r'^p$', 'userprefs'),
    (r'^q$', 'search'),
    (r'^u/(?P<user>\w+)$', 'user'),
    (r'^a/(?P<title>\w+)$', 'article'),
    (r'^e/(?P<title>\w+)$', 'edit'),
    (r'^c/(?P<title>\w+)$', 'category'),
)
