import ply.lex as lex
import re

class TwitterLexer:
    tokens = (
        'atlink',
        'hashlink',
        'weblink',
        'word',
        'punc',
    )

    t_word = u'\w+'
    t_atlink = u'@\w+'
    t_hashlink = u'\#\w+'
    t_weblink = u'(http|ftp|https)://[a-zA-Z0-9\-\.]+/?[a-zA-Z0-9_\-\.\!\~\*\'\(\)\%\?\#]*'
    t_punc = u'\W'

    def __init__(self):
        self.lexer = lex.lex(module=self, reflags=re.UNICODE)

    def t_error(self, t):
        print "Illegal character '%s'" % (t.value[0])
        t.lexer.skip(1)

    def input(self, text):
        self.lexer.input(text)

    def token(self):
        return self.lexer.token()

def twitify(contents):
    if type(contents) is str:
        contents = contents.decode('utf8')

    lexer = TwitterLexer()
    lexer.input(contents)

    processed_contents = ''
    while True:
      token = lexer.token()
      if not token: break
      if token.type == 'atlink':
        processed_contents += '<a href="/a/%s">%s</a>' % (token.value[1:], token.value)
      elif token.type == 'hashlink':
        processed_contents += '<a href="/c/%s">%s</a>' % (token.value[1:], token.value)
      elif token.type == 'weblink':
        processed_contents += '<a href="%s">%s</a>' % (token.value, token.value)
      else:
        processed_contents += token.value

    return processed_contents
