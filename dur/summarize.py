import sys
import re
import pycurl
import StringIO
from mwlib.uparser import simpleparse
import mwlib
import xml.dom.minidom
from xml.dom.minidom import Node

def lookup(title, maxredirects=5):
  if (maxredirects < 0):
    raise Exception("too many redirects: %s (at %s)" % (sys.argv[1], title))
  curl = pycurl.Curl()
  
  curl.setopt(pycurl.URL, (u'http://en.wikipedia.org/wiki/Special:Export/%s' % (title)).encode('utf8'))
  b = StringIO.StringIO()
  curl.setopt(pycurl.WRITEFUNCTION, b.write)
  curl.setopt(pycurl.FOLLOWLOCATION, 1)
  curl.setopt(pycurl.MAXREDIRS, 5)
  curl.perform()
  xmlcontents = b.getvalue()

  doc = xml.dom.minidom.parseString(xmlcontents)
  if not doc.getElementsByTagName("text"):
    print "%s not found" % (sys.argv[1])
    return
  textnode = doc.getElementsByTagName("text")[0]
  if len(textnode.childNodes) != 0:
    match = re.match("#REDIRECT\s*\[\[(.*)\]\]", textnode.childNodes[0].nodeValue)
    if match:
      return lookup(match.group(1), maxredirects=maxredirects-1)
    else:
      contents = textnode.childNodes[0].nodeValue
  else:
    contents = textnode.nodeValue

  contents = re.sub('\{\{[^\}\{]*\}\}', '', contents)
  contents = re.sub('\{\{[^\}\{]*\}\}', '', contents)
  contents = re.sub('<[^><]*>[^<>]*</[^<>]*>', '', contents)
  contents = re.sub('<[^><]*>', '', contents)

  parsed = simpleparse(contents)
  categories = {}
  contents = ''
  for node in parsed.allchildren():
    if len(contents) < 140:
      if type(node) == mwlib.parser.nodes.Text:
        contents += node.text
      #contents += node.
      if type(node) == mwlib.parser.nodes.ArticleLink:
        contents += "@" + re.sub(' ', '_', node.target) + " "
    if type(node) == mwlib.parser.nodes.CategoryLink:
      link = node.target.split(u':')[1]
      categories.setdefault(link, 0)
      categories[link] += 1

  print categories
  return contents

def find_categories(contents):
  pass

#print xmlcontents
def rec(node, deep=0):
  for child in node.childNodes:
    if child != node:
      print ' ' * deep + child.nodeName
      rec(child, deep + 1)
 
#rec(doc)
#page = sys.argv[1]
#print lookup(page)
#lookup(page)

