from django.template import Context, loader
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import datetime
import hashlib
from twitterlexer import twitify
from encyclrcloud import elc, get_lc_key
from summarize import lookup

def get_empty_contents(title):
    try:
        return lookup(title)[:140]
    except:
        return ''

def index(request):
    t = loader.get_template('html/index.html')
    c = Context(request.session)
    return HttpResponse(t.render(c))

def category(request, title):
    articles = [] #elc.list_get(get_lc_key('category', title))
    t = loader.get_template('html/category.html')
    c = Context(request.session)
    c.update({ 'title': title, 'articles': articles})
    return HttpResponse(t.render(c))

def authenticate(request):
    username = request.POST["u"]
    pword = pwordhash(request)
    storedpword = elc.get(get_lc_key("user", username))
    if pword != storedpword:
        return False
    else:
        request.session["user"] = username
        return True

def login(request):
    referer = request.META.get('HTTP_REFERER', '/')

    if not "u" in request.POST or not "p" in request.POST or not authenticate(request):
        request.session["loginfailed"] = True
    elif "loginfailed" in request.session:
        del request.session["loginfailed"]

    return HttpResponseRedirect(referer)
    
def logout(request):
    if 'user' in request.session:
        del request.session['user']
    referer = request.META.get('HTTP_REFERER', '/')
    return HttpResponseRedirect(referer)

def pwordhash(request):
    username = request.POST["u"]
    pword = request.POST["p"]
    return hashlib.sha224(pword + username).hexdigest()
    
def createnewuser(request):
    errors = {}
    if 'u' not in request.POST or request.POST['u'] == '':
        errors['nousername'] = True
    else:
        username = request.POST["u"]
        if elc.get(get_lc_key("user", username)):
            errors["usernametaken"] = username
    if 'p' not in request.POST or 'p2' not in request.POST or request.POST['p'] == '':
        errors['nopassword'] = True
    else:
        pword = request.POST["p"]
        pword2 = request.POST["p2"]
        if pword != pword2:
            errors["passwordmismatch"] = True
    
    if not errors:
       elc.set(get_lc_key("user", username), pwordhash(request))

    return errors 

def newuser(request):
    errors = None
    if request.method == 'POST' or request.method == 'PUT':
        errors = createnewuser(request)
        if not errors:
            authenticate(request)
            return HttpResponseRedirect("/p")
    t = loader.get_template('html/newuser.html')
    c = Context(request.session)
    c.update(errors)

    return HttpResponse(t.render(c))
    
def userprefs(request):
    return HttpResponse('user prefs?')
    
def user(request):
    return HttpResponse('user info?')
    
def article(request, title):
    if request.method == 'POST' or request.method == 'PUT':
        return post(request, title)

    contents = elc.get(get_lc_key('article', title))
    if contents is None:
        contents = 'no contents: please create some'

    t = loader.get_template('html/article.html')

    c = Context(request.session)
    c.update({ 'title':    title,
               'contents': contents })

    return HttpResponse(t.render(c))

def edit(request, title):
    if "user" not in request.session:
        return HttpResponseRedirect("/a/%s" % title)

    contents = elc.get(get_lc_key('rawarticle', title))

    isnew = None
    if contents is None:
        contents = get_empty_contents(title)
        isnew = True

    t = loader.get_template('html/edit.html')
    c = Context(request.session)
    c.update({ 'title':    title,
               'contents': contents.decode('utf8'),
               'isnew':    isnew })

    return HttpResponse(t.render(c))

def post(request, title):
    username = None
    if "user" in request.session:
        username = request.session["user"]
    else:
        return HttpResponseRedirect("/a/%s" % title)

    if "body" not in request.POST:
        raise Exception("no body")

    if len(request.POST["body"]) > 140:
        raise Exception("contents too long (len = %s)" % (len(request.POST["body"])))

    contents = request.POST["body"].encode('utf8')
    processed_contents = twitify(contents).encode('utf8')

    success = elc.set(get_lc_key('article', title), processed_contents)
    if not success:
        raise Exception('error writing to db')

    success = elc.set(get_lc_key('rawarticle', title), contents)
    if not success:
        raise Exception('error writing to db')

    return HttpResponseRedirect("/a/%s" % title)

def search(request):
    if "s" in request.POST:
        return HttpResponseRedirect("/a/%s" % request.POST["s"])
    
    referer = request.META.get('HTTP_REFERER', '/')
    return HttpResponseRedirect(referer)
