import lightcloud

lc_prefices = {
  "user": "u",
  "article": "a",
  "rawarticle": "r",
  "category": "c",
}
def get_lc_key(t, key):
  if type(key) is unicode:
    key = key.encode('utf8')
  prefix = lc_prefices[t]
  return "%s_%s" % (prefix, key)

LIGHT_CLOUD = {
    'lookup1_A': [ '127.0.0.1:41201', '127.0.0.1:51201' ],
    'storage1_A': [ '127.0.0.1:44201', '127.0.0.1:54201' ]
}

lookup_nodes, storage_nodes = lightcloud.generate_nodes(LIGHT_CLOUD)
lightcloud.init(lookup_nodes, storage_nodes)

elc = lightcloud
